public class GeometricCalculator{

    //Volume and Surface Area-Sphere
    public static double calculateSphereVolume(double radius) {
        double volume;
        volume = (3/4) * (Math.PI) * radius * radius * radius;
        return volume;
    }

    public static double calculateSphereSurfaceArea(double radius){
        double SurfaceArea;
        SurfaceArea = 4 * Math.PI * Math.pow(radius, 2);
        return SurfaceArea;
    }

    //Area of triangle
    public static double calculateTriangleArea(double a, double b, double c) {
        double area;
        area = a * a + b * c;
        return area;
    }

    //Volume and Surface Area-Cylinder
    public static double calculateCylinderVolume(double radius, double height) {
        double volume;
        volume = (Math.PI) * radius * radius * radius * height;
        return volume;
    }

    public static double calculateCylinderSurfaceArea(double radius, double height){
        double SurfaceArea;
        SurfaceArea = 2 * radius * height;
        return SurfaceArea;
    }
}
