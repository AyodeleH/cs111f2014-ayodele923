import java.util.Date;
import java.util.Scanner;
import java.text.DecimalFormat;

public class CommandLineGeometer {

    private enum GeometricShape {sphere, triangle, cylinder};

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        GeometricShape shape = GeometricShape.sphere;
        DecimalFormat fmt = new DecimalFormat("0.####");
        double sphereVolume;
        double SurfaceArea;
        double radius;
        double height;
        int x;
        int y;
        int z;
        int s;

        System.out.println("Ayodele Hamilton " + new Date());
        System.out.println("Welcome to the Command Line Geometer!");
        System.out.println();

        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();


        //Calculating the volume and surface area of Sphere
        //Round the output to four decimal places
        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        sphereVolume = (4.0/3.0)*Math.PI * Math.pow(radius, 3);
        System.out.println("The volume is equal to " + fmt.format(sphereVolume));
        System.out.println();

        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        SurfaceArea = (4 * Math.PI * Math.pow(radius, 2));
        System.out.println("The surface area is equal to "+ fmt.format(SurfaceArea));
        System.out.println();


        shape = GeometricShape.triangle;

        System.out.println("What is the length of the first side?");
        x = scan.nextInt();

        System.out.println("What is the length of the second side?");
        y = scan.nextInt();

        System.out.println("What is the length of the third side?");
        z = scan.nextInt();
        System.out.println();

        System.out.println("What is the length of the perimeter?");
        s = scan.nextInt();
        System.out.println();


        //Calculating the area of a triangle
        //Round the output to four decimal places
        System.out.println("Calculating the area of a " + shape + " with sides equal to " + x + y + z );
        double triangleArea = Math.sqrt(x * x + y * z);
        System.out.println("The area is equal to"  + triangleArea);
        System.out.println();

        shape = GeometricShape.cylinder;

        System.out.println("What is the radius for the " + shape + "?");
        radius = scan.nextDouble();
        System.out.println();

        System.out.println("What is the height for the " + shape + "?");
        height = scan.nextDouble();
        System.out.println();


        //Calculating the volume and surface area of a cylinder
        //Round the output to four decimal places
        System.out.println("Calculating the volume of a " + shape + " with radius equal to " + radius);
        double cylinderVolume = Math.PI * Math.pow(radius,2) * height;
        System.out.println("The volume is equal to " + fmt.format(cylinderVolume));
        System.out.println();

        System.out.println("Calculating the surface area of a "+ shape + " with radius equal to " + radius);
        SurfaceArea = 2 * Math.PI * radius * height;
        System.out.println("The surface area is equal to " + fmt.format(SurfaceArea));
        System.out.println();
    }
}
