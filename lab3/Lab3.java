
//****************************************
//HonorCode:The work I am submitting is a result of my own thinking and efforts.
//Ayodele Hamilton 
//CMPSC 111 Fall 2014
// Lab # 3
//Date: 
//
//Purpose: 
//***************************************
import java.util.Date;
import java.util.Scanner;

public class Lab3
{
	//----------------------
	//main metho: program execution begins here 
	//----------------------
	public static void main(String[] args)
	{
	   //Label output with name and date:
	   System.out.println("Ayodele Hamilton \nLab 3\n" + new Date() + "\n");

	   //Declare variables
	   String noun;
	   String greeting; 
	   int bill;
	   int tip;
	   double tip2;
	  // double percentage;
	   int numberOfPeople;
	  double total_bill;
	  double split;	   
	  
	  Scanner scan = new Scanner(System.in);
	  
	  System.out.println("Please Enter Your Name");
	  noun = scan.next();

	  System.out.println(noun + ", Welcome to the Tip Calculator");
	  greeting = scan.nextLine();

	  System.out.println("Please enter the amount of your bill: ");
	  bill = scan.nextInt();

	  System.out.println("Please enter the percentage you want to tip: ");
	  tip = scan.nextInt();
	  tip2 = ((double)tip/100 * bill);
	  
	  System.out.println("Your original bill " +bill);
	  
	  
	  System.out.println("Your tip amount " +tip);

	
	 // System.out.println("Your total bill");
	  total_bill= bill + tip;

	  System.out.println("Your total bill " +total_bill);

	  System.out.println("How many people will be splitting the bill?");
	  numberOfPeople = scan.nextInt();
	  split = (total_bill/numberOfPeople);

	  System.out.println("Each person should pay " +split);
	  

	  System.out.println("Have a nice day! Thank you for using our service.");
	  	   
	}
}
