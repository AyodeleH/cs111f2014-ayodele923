//****************************************
//HonorCode:The work I am submitting is a result of my own thinking and efforts.
//Ayodele Hamilton
//CMPSC 111 Fall 2014
// Lab #
//Date:
//
//Purpose:
//***************************************
import java.util.Date;
import java.util.Scanner;

public class SudokuChecker
{
    Scanner scan = new Scanner(System.in);

    //Variables:
      private int w1,w2,w3,w4;
      private int x1,x2,x3,x4;
      private int y1,y2,y3,y4;
      private int z1,z2,z3,z4;


       //Constructor:
       public SudokuChecker(int ww1,int ww2, int ww3, int ww4,
               int xx1, int xx2, int xx3, int xx4,
               int yy1, int yy2, int yy3, int yy4,
               int zz1, int zz2, int zz3, int zz4)
       {
           w1 = 0;
           w2 = 0;
           w3 = 0;
           w4 = 0;
           x1 = 0;
           x2 = 0;
           x3 = 0;
           x4 = 0;
           y1 = 0;
           y2 = 0;
           y3 = 0;
           y4 = 0;
           z1 = 0;
           z2 = 0;
           z3 = 0;
           z4 = 0;
       }

       public void getGrid()
       {
           System.out.println("Enter row 1: ");
           w1 = scan.nextInt();
           w2 = scan.nextInt();
           w3 = scan.nextInt();
           w4 = scan.nextInt();

           System.out.println("Enter row 2: ");
           x1 = scan.nextInt();
           x2 = scan.nextInt();
           x3 = scan.nextInt();
           x4 = scan.nextInt();

           System.out.println("Enter row 3: ");
           y1 = scan.nextInt();
           y2 = scan.nextInt();
           y3 = scan.nextInt();
           y4 = scan.nextInt();

           System.out.println("Enter row 4: ");
           z1 = scan.nextInt();
           z2 = scan.nextInt();
           z3 = scan.nextInt();
           z4 = scan.nextInt();
        }

       public void checkGrid()
       {
           boolean k = true;

            if (w1 + w2 + w3 + w4 == 10){
                System.out.println("Good");
            }else
            {
                System.out.println("Not Good");
                k = false;
            }

            if (x1 + x2 + x3 + x4 == 10){
                System.out.println("Good");
            }else
            {
                System.out.println("Not Good");
                k = false;
            }

            if (y1 + y2 + y3 + y4 == 10){
                System.out.println("Good");
            }else
            {
                System.out.println("Not Good");
                k = false;
            }

            if (z1 + z2 + z3 + z4 == 10){
                System.out.println("Good");
            }else
            {
                System.out.println("Not Good");
                k = false;
            }

            if (k == true){
                System.out.println("Sudoku grid is valid");
            }else
            {
                System.out.println("Sudoku is not valid");
            }
        }
}





