Ayodele Hamilton 
Lab #8
Wed Nov 05 22:11:03 EST 2014

Welcome to the Sudoku Checker!
This program checks simple and small 4x4 Sudoku grids for correctness.
Each column, row and 2x2 region contains the numbers 1 through 4 only once.
To check your Sudoku, enter your board one row at a time, 
 with each digit seperate by a space. Hit ENTER at the end of the row.
Enter row 1: 
1
2
3
4
Enter row 2: 
4
3
2
1
Enter row 3: 
3
2
1
4
Enter row 4: 
2
3
1
4
Good
Good
Good
Good
Sudoku grid is valid



