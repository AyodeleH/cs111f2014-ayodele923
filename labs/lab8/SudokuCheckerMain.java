//****************************************
//HonorCode:The work I am submitting is a result of my own thinking and efforts.
//Ayodele Hamilton
//CMPSC 111 Fall 2014
// Lab # 8
//Date: 11/5/14
//
//Purpose: To create a sudoku game.
//***************************************
import java.util.Date;

public class SudokuCheckerMain
{
	//----------------------
	//main method: program execution begins here
	//----------------------
	public static void main(String[] args)
	{
	   //Label output with name and date:
	   System.out.println("Ayodele Hamilton \nLab #8\n" + new Date() + "\n");

	   SudokuChecker checker = new SudokuChecker(0,0,0,0,
               0,0,0,0,
               0,0,0,0,
               0,0,0,0);

       System.out.println("Welcome to the Sudoku Checker!");

       System.out.println("This program checks simple and small 4x4 Sudoku grids for correctness.");
       System.out.println("Each column, row and 2x2 region contains the numbers 1 through 4 only once.");

       System.out.println("To check your Sudoku, enter your board one row at a time, ");
       System.out.println(" with each digit seperate by a space. Hit ENTER at the end of the row.");


       checker.getGrid();
       checker.checkGrid();
    }
}
