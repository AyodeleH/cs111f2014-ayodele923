import java.io.IOException;
import java.util.Scanner;
import java.util.Iterator;

public class TodoListMain {

    public static void main(String[] args) throws IOException {
        System.out.println("Welcome to the Todo List Manager!");
        System.out.println("What operation would you like to perform?");
        System.out.println("Available options: read, priority-search, category-search, done, list, quit");

        Scanner scanner = new Scanner(System.in);
        TodoList todoList = new TodoList();


        while(scanner.hasNext()) {
            String command = scanner.nextLine();
            if(command.equals("read")) {
                todoList.readTodoItemsFromFile();
            }
            if(command.equals("list")) {
                System.out.println(todoList.toString());
            }
            if (command.equals("priority-search")){
                System.out.println("Are you looking for A or B?");
                String requestedPriority = scanner.nextLine();
                todoList.findTasksOfPriority(requestedPriority.toString());
                Iterator iterator = todoList.findTasksOfPriority(requestedPriority);
                while(iterator.hasNext()){
                    System.out.println(iterator.next());
                }
            }
            if (command.equals("category-search")){
                System.out.println("Are you looking for Understand or Explain?");
                String requestedCategory = scanner.nextLine();
                todoList.findTasksOfCategory(requestedCategory.toString());
                //String cat = todoList.findTasksOfCategory(requestedCategory).toString();
                //System.out.println(cat);
                Iterator iterator = todoList.findTasksOfCategory(requestedCategory);
                while(iterator.hasNext()){
                        System.out.println(iterator.next());
                }


            }
            else if(command.equals("done")) {
                System.out.println("What is the id of the task?");
                int chosenId = scanner.nextInt();
                todoList.markTaskAsDone(chosenId);
            }
            else if(command.equals("quit")) {
                break;
            }
        }

    }

}
