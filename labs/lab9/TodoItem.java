import java.util.Iterator;

public class TodoItem {

    private int id;
    private static int nextId = 0;
    private String priority;
    private String category;
    private String task;
    private boolean done;

    public TodoItem(String p, String c, String t) {
        id = nextId;
        nextId++;
        priority = p;
        category = c;
        task = t;
        done = false;
    }

    public int getId() {
        return id;
    }

    public String getPriority() {
        return priority;
    }

    public String getCategory() {
        return category;
    }

    public String getTask() {
        return task;
    }

    public void markDone() {
        done = true;
    }

    public boolean isDone() {
        return done;
    }

    public String toString() {
        return new String(id + ", " + priority + ", " + category + ", " + task + ", done? " + done);
    }

}
