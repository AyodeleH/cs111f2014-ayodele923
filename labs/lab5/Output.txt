hamiltona@aldenv138:~/cs111F2014/cs111F2014-Ayodele923/labs/lab5$ java Lab5Ayodele Hamilton and Daniel Bonnett 
Lab #5
Wed Oct 08 18:54:35 EDT 2014

Enter DNA string
acgt
Complement of ACGT is TGCA
Inserting A at position 4 gives ACGTA
Deleting from position 0 gives CGT
Changing position 1 gives AGCGT




hamiltona@aldenv138:~/cs111F2014/cs111F2014-Ayodele923/labs/lab5$ java Lab5Ayodele Hamilton and Daniel Bonnett 
Lab #5
Wed Oct 08 18:58:35 EDT 2014

Enter DNA string
gtca
Complement of GTCA is CAGT
Inserting T at position 4 gives GTCAT
Deleting from position 0 gives TCA
Changing position 2 gives GTCCA





hamiltona@aldenv138:~/cs111F2014/cs111F2014-Ayodele923/labs/lab5$ java Lab5Ayodele Hamilton and Daniel Bonnett 
Lab #5
Wed Oct 08 18:59:15 EDT 2014

Enter DNA string
cgtaat
Complement of CGTAAT is GCATTA
Inserting C at position 6 gives CGTAATC
Deleting from position 4 gives CGTAT
Changing position 4 gives CGTAGAT

