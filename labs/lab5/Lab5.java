//****************************************
//HonorCode:The work I am submitting is a result of my own thinking and efforts.
//Ayodele Hamilton and Daniel Bonnett
//CMPSC 111 Fall 2014
// Lab #5
//Date:10/2/14
//
//Purpose: DNA Manipulation
//***************************************
import java.util.Random;
import java.util.Date;
import java.util.Scanner;

public class Lab5
{
	//----------------------
	//main method: program execution begins here
	//----------------------
	public static void main(String[] args)
	{
	   //Label output with name and date:
	   System.out.println("Ayodele Hamilton and Daniel Bonnett \nLab #5\n" + new Date() + "\n");

	   // Variable dictionary:
       Scanner scan = new Scanner(System.in);
       Random r = new Random();
       String dnaString;
       String mutation1, mutation2, mutation3 ,mutation4;

       System.out.println("Enter DNA string");
       dnaString = scan.nextLine();
       dnaString=dnaString.toUpperCase();

       //This is going to print out the complement of the DNA string
       String dnaCompString=dnaString;

       dnaCompString = dnaCompString.replace('A','t');
       dnaCompString = dnaCompString.replace('T','a');
       dnaCompString = dnaCompString.replace('C','g');
       dnaCompString = dnaCompString.replace('G','c');

       dnaCompString = dnaCompString.toUpperCase();
       System.out.println("Complement of " + dnaString + " is " + dnaCompString);


       int randomNumber = r.nextInt(4);
       char randomLetter = "ATCG".charAt(randomNumber);

       //This inserts a random letter.
       mutation1 = dnaString + randomLetter;
       System.out.println("Inserting " + randomLetter + " at position " + dnaString.length() + " gives " + mutation1);

       //This deletes a random a letter
       int randomIndex = r.nextInt(dnaString.length());
       mutation2 = dnaString.substring(0,randomIndex)+dnaString.substring(randomIndex+1);
       System.out.println("Deleting from position " + randomIndex + " gives " + mutation2);

       //This changes a random letter
       randomIndex = r.nextInt(dnaString.length());
       randomLetter = "ATCG".charAt(r.nextInt(4));
       mutation4 = dnaString.substring(0,randomIndex)+randomLetter+dnaString.substring(randomIndex);
       System.out.println("Changing position " + randomIndex + " gives " + mutation4);
	}
}

