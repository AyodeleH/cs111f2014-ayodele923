//=================================================
//HonorCode:The work I am submitting is a result of my own thinking and efforts.
//Ayodele Hamilton 
//CMPSC 111 Fall 2014
// Lab # 4
//Date: 9/25/14
//
//Purpose: 
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet
{
    //-------------------------------------------------
    // Use Graphics methods to add content to the drawing canvas
    //-------------------------------------------------
    public void paint(Graphics page) {
    	final int WIDTH = 600;
	final int HEIGHT = 400;
	
	//paint the background 
	page.setColor (Color.darkGray);
	page.fillRect(0,0,WIDTH,HEIGHT);

	//Paint the Dirt 
	page.setColor (Color.green);
	page.fillRect(0,350,WIDTH,HEIGHT);
	
	//Paint Tree Trunk 
	page.setColor(Color.black);
	page.fillRect(242,210,40,170);
	
	//Paint the Leaves
	page.setColor(Color.red);
	page.fillOval(220,110,135,135);
	page.fillOval(170,110,135,135);
	page.fillOval(192,110,135,127);

	//Draw Leaves 
	page.setColor(Color.orange);
	page.drawArc(210,170,60,60,180,95);
	page.drawArc(255,130,60,60,278,100);
	page.drawArc(255,170,60,60,265,100);
	page.drawArc(210,130,60,60,185,100);

	//Paint Hole in Tree Trunk 
	page.setColor(Color.yellow);
	page.fillOval(245,280,25,35);

	//Draw Egg
	page.setColor(Color.white);
	page.fillOval(240,65,55,55);

	//Draw Rain
	page.setColor(Color.blue);
	page.drawLine(0,0,0,380);
	page.drawLine(50,50,50,380);
	page.drawLine(100,100,100,200);
	page.drawLine(150,150,150,25);
	page.drawLine(200,200,200,380);
	page.drawLine(300,300,300,200);
	page.drawLine(400,400,400,100);
	page.drawLine(500,500,500,80);
	page.drawLine(550,550,550,20);

	//Draw Clouds 
	page.setColor(Color.white);
	page.fillOval(380,20,120,20);
	page.fillOval(100,20,120,20);
	page.fillOval(200,20,120,20);
	page.fillOval(50,20,120,20);
	page.fillOval(300,20,120,20);
	page.fillOval(500,20,120,20);
	
	}


        

    
}
