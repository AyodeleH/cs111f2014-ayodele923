
//****************************************
//HonorCode:The work I am submitting is a result of my own thinking and efforts.
//Ayodele Hamilton 
//CMPSC 111 Fall 2014
// Lab 2
//Date: September 11,2014
//
//Purpose: to compute and print different types of music that are worth listening to.
//***************************************
import java.util.Date;

public class Lab2
{
	//----------------------
	//main method: program execution begins here 
	//----------------------
	public static void main(String[] args)
	{
	   //Label output with name and date:
	   System.out.println("Ayodele Hamilton \nLab 2\n" + new Date() + "\n");

	   // Variable directionary:  
	   int inspirationToNotes = 4/4;   //life inspiration creates notes 
	   int notesToStaff = 100;       //notes are put onto paper
	   int musicalnotesToInstruments = 1600 ;  //Music is created by instruments
	   int numberOfInstruments = 16;  // All instruments used
	   int harmonyToEar; 		//Something to love
	   // Compute values:
	   harmonyToEar = inspirationToNotes * notesToStaff * musicalnotesToInstruments - numberOfInstruments;
	   
	   System.out.println("life inspiration creates notes: " + inspirationToNotes);
	   System.out.println("notes are put onto paper: " + notesToStaff);
	   System.out.println("Music is created by instruments: " + musicalnotesToInstruments); 
	   System.out.println("Something to love: "  + harmonyToEar);
	}
}

	
