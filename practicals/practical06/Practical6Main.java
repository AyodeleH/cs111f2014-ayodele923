//*****************************
// CMPSC 111
// Practical 6
// October 29, 2014
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;
public class Practical6Main
{
    public static void main ( String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.println("Please enter a year between 1000 and 3000!");
        userInput = scan.nextInt();

        YearChecker activities = new YearChecker(userInput);

        if (activities.isLeapYear() == true)
        {   System.out.println(userInput + " is a leap year"); }
        else {System.out.println(userInput + " is not a leap year");}

        if (activities.isCicadaYear() == true)
        {   System.out.println(userInput + " is an emergence year for Brood II of the 17-year cicadas"); }
        else {System.out.println(userInput + " is not an emergence year for Brood II of the 17-year cicadas ");}

        if (activities.isSunspotYear() == true)
        {   System.out.println(userInput + " is a peak sunspot year."); }
        else {System.out.println(userInput + " is not a peak sunspot year. ");}

        System.out.println("Thank you for using this program.");
    }
}
