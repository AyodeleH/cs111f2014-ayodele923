Ayodele Hamilton
October 2, 2014

1. Do you recognize the names of any of the Gvim plug ins as they are being installed? What do they do?
    I don't recognize any of the plug ins being installed.

2. What are five ways in which it is now different from the "default" configuration that you were using previously?
    Five different plug ins are the NerdTree, the color scheme, the comments, line numbers, eas access to  files.

3. How does your chosen plug in work? What features does it provide? Do you plan to use this plug in on a regular basis? Why or why not?
    The plug in I chose works to help make the program look readable and it helps to distingusih the different points of the code written. It provides new color and easy access to files. I do plan to use this plug in on a regular because it is efficient.

4. What do you think the "gg=G" command does?
    This command made the Gvim screen tint.

5. How do these commands allow you to manipulate your Java program?
    These commands help you to manipulate  the Java program by giving shortcuts to the program to make it helpful. The commands help the Java program to run efficiently and user friendly.
