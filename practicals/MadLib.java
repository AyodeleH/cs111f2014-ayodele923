//****************************************
//HonorCode:The work I am submitting is a result of my own thinking and efforts.
//Ayodele Hamilton 
//CMPSC 111 Fall 2014
// Practical 
//Date: 9/18/14
//
//Purpose: 
//***************************************
import java.util.Date;
import java.util.Scanner;

public class MadLib
{
	//----------------------
	//main metho: program execution begins here 
	//----------------------
	public static void main(String[] args)
	{
	   //Label output with name and date:
	   System.out.println("Ayodele Hamilton \nLab #\n" + new Date() + "\n");
	   
	   //Declare variables 
	   String noun;
	   String adjective1;
	   String adjective2;
	   String non_zero_whole_number;
	   String another_non_zero_whole_number;
	   String any_number;
	   String singular_verb;

	  Scanner scan = new Scanner(System.in);
	  
	  System.out.println("Enter the noun");
	  noun = scan.next();

	  System.out.println("Enter the adjective");
	  adjective1 = scan.next();

	  System.out.println("Enter the another adjective");
	  adjective2 = scan.next();

	  System.out.println("Enter the non-zero whole number");
	  non_zero_whole_number = scan.next();

	  System.out.println("Enter the another non- zero whole number");
	  another_non_zero_whole_number = scan.next();

	  System.out.println("Enter any number");
	  any_number = scan.next();
	
	  System.out.println("Enter singular verb");
	  singular_verb = scan.next();
	 
	System.out.println("If you have " + non_zero_whole_number + adjective1 + noun);
	System.out.println("in a basket, and you take out " + another_non_zero_whole_number + adjective2);
	System.out.println("how many " + noun);
	System.out.println(" are left " + singular_verb);

	}
}
